FROM node AS build
COPY . /app
WORKDIR /app/src
RUN mv .env.example .env && \
    npm install && \
    npm run unit_test

FROM node
WORKDIR /app/src
COPY --from=build /app/src . 
RUN mkdir -p ../third_party/projects
EXPOSE 8000
ENTRYPOINT [ "npm", "start" ]
