pipeline {
  agent any
  environment{
    TAG_VERSION = 1.0
    IMAGE = 'compiler'
    DOCKER_HUB_USER = 'briansantivanez'
    DOCKER_HUB_PASSWORD = credentials('docker_hub_pass')
    SONAR_TOKEN = credentials('sonar_token')
    DESTINATION_EMAIL = "brian.santivanez@jala-foundation.org"
  }
  stages {
    stage('commit') {
      steps {
        sh 'docker build -t ${IMAGE}:${TAG_VERSION} .'
        sh 'docker rm -f dummy'
        sh 'docker create -ti --name dummy ${IMAGE}:${TAG_VERSION} bash'
        sh 'docker cp dummy:/app/src/test-results.xml .'
      }
      post {
        always {
          archiveArtifacts 'test-results.xml'
          archiveArtifacts 'docker-compose.yml'
          sh 'docker rm -f dummy'
          copyArtifacts filter: 'test-results.xml', fingerprintArtifacts: true, projectName: '${JOB_NAME}', selector: specific('${BUILD_NUMBER}')
          junit 'test-results.xml'
        }
      }
    }

    stage('publish') {
      steps {
        sh 'docker login -u "${DOCKER_HUB_USER}" -p "${DOCKER_HUB_PASSWORD}"'
        sh 'docker tag ${IMAGE}:${TAG_VERSION} briansantivanez/${IMAGE}:${TAG_VERSION}'
        sh 'docker push briansantivanez/${IMAGE}:${TAG_VERSION}'
      }
    }

    stage('quality') {
      steps {
        sh '/usr/local/bin/sonar-scanner-4.4.0.2170-linux/bin/sonar-scanner -Dsonar.organization=briansantivanez1 -Dsonar.projectKey=compiler_service -Dsonar.sources=. -Dsonar.host.url=https://sonarcloud.io'
      }
    }

    stage('deploy') {
      steps {
        copyArtifacts filter: 'docker-compose.yml', fingerprintArtifacts: true, projectName: '${JOB_NAME}', selector: specific('${BUILD_NUMBER}')
        sh 'docker-compose up -d'
      }
    }

    stage('acceptance') {
      steps {
        sh 'echo "acceptance stage"'
      }
    }

  }
}