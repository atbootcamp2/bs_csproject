/*
 @utils_file_controller.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


const EmptyOrNullValidation = require("./../validation_strategy/empty_or_null_validation");
const FolderOrFileDoesntExistValidation = require("./../validation_strategy/path_doesnt_exist_validation");
const ContextValidation = require("./../validation_strategy/context_validation");


class ValidationFileContoller {
    
    // Validates data save_file
    validate_save_file_data(file_name, project_id, content) {
        const facade_strategies = [
            new EmptyOrNullValidation(project_id, "Project Id"),
            new EmptyOrNullValidation(file_name, 'File Name'),
            new EmptyOrNullValidation(content, 'Content')
        ];
        const context = new ContextValidation(facade_strategies);
        context.validate();
    }

    // Validates path save_file
    validate_save_file_path(file_path) {
        const facade_strategies = [
            new EmptyOrNullValidation(file_path, "Path file"),
            new FolderOrFileDoesntExistValidation(file_path, 'File')
        ];
        const context = new ContextValidation(facade_strategies);
        context.validate();
    }

    // Validates content update_file
    validate_update_file(content) {
        const facade_strategies = [
            new EmptyOrNullValidation(content, 'Content')
        ];
        const context = new ContextValidation(facade_strategies);
        context.validate();
    }
}

// Exports class
module.exports = ValidationFileContoller;
