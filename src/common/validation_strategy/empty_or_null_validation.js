/*
 @empty_or_null_validation.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


const IValidator = require("./i_validator");
const CompilersServiceError = require('./../errors/compilers_service_error');


// builds EmptyOrNullValidation class
class EmptyOrNullValidation extends IValidator {

    // Defines the constructor
    constructor(value, field) {
        super();
        this.value = value;
        this.field = field;
    }

    // Validates if the value is empty or null
    validate() {
        if (typeof(this.value) == 'undefined' || this.value == null || this.value == '') {
            throw new CompilersServiceError(this.field + " in null or empty");
        }
    }
}

// Exports EmptyOrNullValidation class
module.exports = EmptyOrNullValidation;
