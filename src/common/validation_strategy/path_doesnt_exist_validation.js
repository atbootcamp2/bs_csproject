/*
 @path_doesnt_exist_validation.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


const IValidator = require("./i_validator");
const fs = require('fs');
const CompilersServiceError = require('./../errors/compilers_service_error');


class FolderOrFileDoesntExistsValidation extends IValidator {

    // Defines the constructor
    constructor(path, object_name) {
        super();
        this.path = path;
        this.object_name = object_name;
    }

    // Validates if the folder or file doesn't exist
    validate() {
        if(fs.existsSync(this.path)) {
            throw new CompilersServiceError(this.object_name+" already exists");
        }
    }
}

// Exports FolderOrFileDoesntExistsValidation class
module.exports = FolderOrFileDoesntExistsValidation;
