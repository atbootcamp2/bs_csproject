/*
 @constants.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


const dotenv = require('dotenv');
dotenv.config();
const java_path = process.env.BINARY_PATH_JAVA;
const python_path = process.env.BINARY_PATH_PYTHON;
const node_path = process.env.BINARY_PATH_NODE;
const csharp_path = process.env.BINARY_PATH_CSHARP;

// Constants used in the project
const projects_path = process.env.PROJECTS_PATH;
const languages = {
    java: {
        type: '.java',
        cabecera: '',
        main_content: ';\npublic class main{\n\tpublic static '+
            'void main(String[] args){\n\t\tSystem.out.println("compilers service ' +
            'says hello from Java...!!!");\n\t}\n}\n',
        path: java_path
    },
    python: {
        type: '.py',
        cabecera: '# ',
        main_content: "\nif __name__ == '__main__':\n\tprint('compilers service says hello" +
        " from Python...!!!')\n",
        path: python_path
    },
    node: {
        type: '.js',
        cabecera: '// ',
        main_content: "\nconsole.log('compilers service says hello" +
        " from NodeJS...!!!')\n",
        path: node_path
    },
    csharp: {
        type: '.cs',
        cabecera: '//',
        main_content: "\nusing System;\nnamespace MyNamespace {\n\tpublic class MyMainClass {\n\t\tstatic void Main()"+
        "{\n\t\t\tConsole.WriteLine(\"compilers service says hello from CSharp...!!!\");\n\t\t}\n\t}\n}\n",
        path: csharp_path
    }
};

// Exports constants
module.exports = {
    projects_path,
    languages
};
