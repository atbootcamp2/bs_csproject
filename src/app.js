/*
 @compilers_service.py Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/

'use strict';

var express = require('express');
var body_parser = require('body-parser');
var app = express();

// parse body
app.use(body_parser.urlencoded({extended:false}));
app.use(body_parser.json());

// load routes
var project_routes = require('./routes/project_routes');
var file_routes = require('./routes/file_routes');
var console_routes = require('./routes/console_routes');

// configure http headers
app.use((req,res,next)=>{
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-control-Allow-Headers','Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods','GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow','GET, POST, OPTIONS, PUT, DELETE');

    next();
})

// routes
app.use('/api/v1/', project_routes);
app.use('/api/v1/', file_routes);
app.use('/api/v1/', console_routes);

// export app
module.exports = app;
