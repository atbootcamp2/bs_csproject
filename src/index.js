/*
 @index.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/

'use strict';

const dotenv = require('dotenv');
const mongoose = require('mongoose');
const app = require('./app');
dotenv.config();
const port = process.env.APP_PORT || 8000;
const data_base = process.env.DB_DATABASE;

// initiate project
mongoose.connect (process.env.DB_CONNECTION+':'+process.env.DB_PORT+'/'+
    data_base, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    },
    (err, res)=>{
        if (err) {
            throw err;
        } else {
            console.log("Connected to "+data_base);
            app.listen(port,function(){
                console.log("Compilers service initiated at "+process.env.APP_URL+":"+port);
            })
        }
    }
);
