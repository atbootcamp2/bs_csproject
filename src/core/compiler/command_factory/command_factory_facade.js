/*
@command_factory_facade.js Copyright (c) 2021 Jalasoft
2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
All rights reserved
This software is the confidential and proprietary information of
Jalasoft , Confidential Information "). You shall not
disclose such Confidential Information and shall use it only in
accordance with the terms of the license agreement you entered into
with Jalasoft
*/


// Imports necessary classes.
const CommandFactory = require('./command_factory');
const Parameters = require('./parameters');
const Executer = require('../../common/executer');
const ValidationCore = require('../../../common/validation_strategy/validation_core');
const Results = require('../../results');

// Builds the Facade class
class Command_facade {

    // Creates and runs the project
    async run_project(path_binary, path_projects, name_project, language) {
        const validate = new ValidationCore();
        validate.facade_parameters(path_binary, path_projects, name_project, language);
        const parameters = new Parameters(path_binary, path_projects, name_project);
        let command_factory = new CommandFactory();
        const command_language = command_factory.get_command(language);
        const built_commands = command_language.builder(parameters);
        const executer = new Executer();
        const command = await executer.run(built_commands);
        return new Results(command.pid, command.stdout);
    }     
}

// Exports Facade class 
module.exports = Command_facade