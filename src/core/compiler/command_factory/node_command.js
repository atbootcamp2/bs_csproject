/*
@node_command.js Copyright (c) 2021 Jalasoft
2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
All rights reserved
This software is the confidential and proprietary information of
Jalasoft , Confidential Information "). You shall not
disclose such Confidential Information and shall use it only in
accordance with the terms of the license agreement you entered into
with Jalasoft
*/


// Import Command from './command'
const Command = require('../../common/executer');
const ValidationCore = require('../../../common/validation_strategy/validation_core');


// builds the NodeCommand class
class NodeCommand extends Command {

    // defines the constructor for the class
    constructor() {
        super()
    }

    // Returns a array with the necessary commands tu run project
    builder(parameters) {
        const validate = new ValidationCore();
        validate.command_parameters(parameters);
        let runner_command = {
            main_command: `${parameters.get_path_binary()}`,
            arguments_list: [`${parameters.get_path_projects()}/${parameters.get_name_project()}/main.js`]
        };
        return [runner_command]; 
    }     
}

// Exports PythonCommand class
module.exports = NodeCommand