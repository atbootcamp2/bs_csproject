/*
@commnad_executer.js Copyright (c) 2021 Jalasoft
2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
All rights reserved
This software is the confidential and proprietary information of
Jalasoft , Confidential Information "). You shall not
disclose such Confidential Information and shall use it only in
accordance with the terms of the license agreement you entered into
with Jalasoft
*/


const CompilersServiceError = require('../../common/errors/compilers_service_error');
const child_process = require('child_process');
const child_process_options = {
        cwd: process.cwd(),
        env: process.env,
        stdio: 'pipe',
        encoding: 'utf-8'
    };

class Executer {

    // Runs an array of commands and shows the last result.   
    async run(commands) {
        let result = {};
        try {
            for (let index = 0; index < commands.length; index++) {
                const command_line = commands[index];
                const child = child_process.spawnSync(
                    command_line['main_command'],
                    command_line['arguments_list'],
                    child_process_options);
                result.pid = child.pid;
                if (child.stderr.length == 0){
                    result.stdout = child.stdout;
                } else {
                    result.stdout = child.stderr;
                    break;
                }
            }
            return result;
        } catch (error) {
            throw new CompilersServiceError("error " + error.message);
        }
    }
}

// Exports Command class
module.exports = Executer
