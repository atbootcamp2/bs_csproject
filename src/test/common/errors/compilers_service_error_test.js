/*
 @compilers_service_error_test.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


var expect = require("chai").expect;
const CompilersServiceError = require('../../../common/errors/compilers_service_error');


describe('compilers service error test', () => {

    // Positive test from CompilersServiceError
    it("Create a CompilersServiceError", () => {
        expect(() => { throw new CompilersServiceError('CompilersServiceError test'); }).to.throw(CompilersServiceError, 'CompilersServiceError test');
    });
});
