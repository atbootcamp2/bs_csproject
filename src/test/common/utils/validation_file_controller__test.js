/*
 @utils_file_controller_test.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


var expect = require("chai").expect;
const ValidationFileController = require('../../../common/utils/validation_file_controller');
const CompilersServiceError = require('../../../common/errors/compilers_service_error');


describe('validation file controller test', () => {

    // Negative tests from validate_save_file_data()
    it("Send null as name_file in validate_save_file_data()", () => {
        let validation_file_controller = new ValidationFileController();
        expect(() => { validation_file_controller.validate_save_file_data(null, '6048d8edf6f623361c4273e0', "print ('hola mundo')"); }).to.throw(CompilersServiceError, 'File Name in null or empty');
    });
    
    it("Send null as project_id in validate_save_file_data()", () => {
        let validation_file_controller = new ValidationFileController();
        expect(() => { validation_file_controller.validate_save_file_data('main', null, "print ('hola mundo')"); }).to.throw(CompilersServiceError, 'Project Id in null or empty');
    });
    
    it("Send null as content in validate_save_file_data()", () => {
        let validation_file_controller = new ValidationFileController();
        expect(() => { validation_file_controller.validate_save_file_data('main', '6048d8edf6f623361c4273e0', null); }).to.throw(CompilersServiceError, 'Content in null or empty');
    });
    
    it("Don't send content as parameter in validate_save_file_data()", () => {
        let validation_file_controller = new ValidationFileController();
        expect(() => { validation_file_controller.validate_save_file_data('main', '6048d8edf6f623361c4273e0'); }).to.throw(CompilersServiceError, 'Content in null or empty');
    });
    
    it("Don't send content and project_id as parameters in validate_save_file_data()", () => {
        let validation_file_controller = new ValidationFileController();
        expect(() => { validation_file_controller.validate_save_file_data('main'); }).to.throw(CompilersServiceError, 'Project Id in null or empty');
    });
    
    it("Don't send any parameter in  validate_save_file_data()", () => {
        let validation_file_controller = new ValidationFileController();
        expect(() => { validation_file_controller.validate_save_file_data(); }).to.throw(CompilersServiceError, 'Project Id in null or empty');
    });
    
    // Negative test from validate_save_file_path()
    it("Don't send content as parameter in validate_save_file_path()", () => {
        let validation_file_controller = new ValidationFileController();
        expect(() => { validation_file_controller.validate_save_file_path(); }).to.throw(CompilersServiceError, 'Path file in null or empty');
    });

    // Negative test from validate_update_file()
    it("Don't send content as parameter in validate_update_file()", () => {
        let validation_file_controller = new ValidationFileController();
        expect(() => { validation_file_controller.validate_update_file(); }).to.throw(CompilersServiceError, 'Content in null or empty');
    });
});
