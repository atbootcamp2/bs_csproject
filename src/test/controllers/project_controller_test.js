/*
 @project_controller_test.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


const expect = require("chai").expect;
const request = require('supertest');
const assert = require('assert');
const app = require('../../app');
const conn = require('../../index');
const constants = require('./../../common/constants/constants');
const Utils = require('./../../common/utils/utils');
const utils = new Utils();
const ProjectController = require('./../../controllers/project_controller');
const project_controller = new ProjectController();


describe('project controller test', () => {

    // Positive test from project controller to get all projects
    it("get status of get_projects()", (done) => {
        request(app).get('/api/v1/project')
        .then((res) => {
            const status = res.status;
            assert.deepStrictEqual(status, 200);
            done();
        }).catch((err) =>{});
    });

    // Negative test from project controller to get a project
    it("get status of get a project with a correct id", (done) => {
        request(app).get('/api/v1/project/6048e58ea28')
        .then((res) => {
            const status = res.status;
            assert.deepStrictEqual(status, 400);
            done();
        }).catch((err) =>{});
    });

    // Positive test from project controller to get a project
    it("get status of get a project with a correct id", (done) => {
        request(app).get('/api/v1/project/605b64bbbba34d23b8d0c338')
        .then((res) => {
            const status = res.status;
            assert.deepStrictEqual(status, 200);
            done();
        }).catch((err) =>{});
    });

    // Negative tests from project controller to save a project
    it("save project with null values", (done) => {
        request(app).post('/api/v1/project')
        .send({
            name : null,
            language : null
        })
        .then((res) => {
            const body = res.body;
            expect(body).to.contain.property('error');
            assert.deepStrictEqual(body['error'], 'Project name in null or empty');
            done();
        }).catch((err) =>{});
    });
    
    it("save project with invalid language", (done) => {
        request(app).post('/api/v1/project')
        .send({
            name : 'testProject',
            language : null
        })
        .then((res) => {
            const body = res.body;
            expect(body).to.contain.property('error');
            assert.deepStrictEqual(body['error'], 'Language in null or empty');
            done();
        }).catch((err) =>{});
    });

    // Positive test from project controller to save a project
    it("save project with correct values", (done) => {
        request(app).post('/api/v1/project')
        .send({
            name : 'testProject',
            language : 'java'
        })
        .then((res) => {
            const body = res.body;
            const project_path = constants.projects_path + '/' + body.name;
            utils.delete_path(project_path);
            const req = {
                params: {
                    id: body._id 
                }
            }
            project_controller.delete_project(req, res);
            expect(body).to.contain.property('name');
            assert.deepStrictEqual(body['name'], 'testProject');
            done();
        }).catch((err) =>{});
    });

    // Negative test from project controller to delete a project
    it("delete project with a incorrect id", (done) => {
        request(app).delete('/api/v1/project/60478de')
        .then((res) => {
            const body = res.body;
            expect(body).to.contain.property('error');
            assert.deepStrictEqual(body['error'], "Project doesn't exist in database");
            done();
        }).catch((err) =>{});
    });

    // Positive test from project controller to delete a project
    it("delete project with a correct id", (done) => {
        request(app).post('/api/v1/project')
        .send({
            name : 'testProject',
            language : 'java'
        })
        .then((res) => {
            const body = res.body;
            request(app).delete('/api/v1/project/' + body._id)
            .then((res) => {
                const body = res.body;
                expect(body).to.contain.property('name');
                assert.deepStrictEqual(body['name'], 'testProject');
                done();
            }).catch((err) =>{});
        }).catch((err) =>{});
        
    });

    //Negative test from project controller to get a project
    it("get project with a incorrect id", (done) => {
        request(app).get('/api/v1/project/60478de')
        .then((res) => {
            const body = res.body;
            expect(body).to.contain.property('error');
            assert.deepStrictEqual(body['error'], "Project doesn't exist in database");
            done();
        }).catch((err) =>{});
    });
});
