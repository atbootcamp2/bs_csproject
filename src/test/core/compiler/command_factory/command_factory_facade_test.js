/*
@command_factory_facade_test.js Copyright (c) 2021 Jalasoft
2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
All rights reserved
This software is the confidential and proprietary information of
Jalasoft , Confidential Information "). You shall not
disclose such Confidential Information and shall use it only in
accordance with the terms of the license agreement you entered into
with Jalasoft
*/


const assert = require('assert');
const Command_facade = require('../../../../core/compiler/command_factory/command_factory_facade');
const CompilersServiceError = require('../../../../common/errors/compilers_service_error');


describe('command factory facade test', () => {

    it("validate null parameters ", async() => {
        let command_facade = new Command_facade();
        const expected_error = new CompilersServiceError('binary path in null or empty');
        let result_error = null;
        try {
           await command_facade.run_project(null, null, null, null);
        } catch (err) {
            result_error = err;
        }
        assert.deepStrictEqual(expected_error, result_error);
    });
});