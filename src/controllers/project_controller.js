/*
 @project_controller.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


'use strict'

const project_model = require('../models/project_model');
const file_model = require('../models/file_model');
const constants = require('./../common/constants/constants');
const Utils = require('./../common/utils/utils');
const utils = new Utils();
const ValidationProjectContoller = require('./../common/utils/validation_project_controller');
const validation_project_controller = new ValidationProjectContoller();


class ProjectController {
    
    // Shows all projects
    async get_projects(req, res) {
        try {
            const projects = await project_model.find({});
            res.json(projects);
        } catch (err) {
            res.status(400).json({error: err.message});
        }
    }

    // Gets one project
    async get_project_by_id(req, res) {
        try {
            const project_id = req.params.id;
            const project = await project_model.findById(project_id);
            res.json(project);
        } catch (err) {
            if(err.kind == 'ObjectId') {
                err.message = "Project doesn't exist in database";
            }
            res.status(400).json({error: err.message});
        }
    }

    // Saves project
    async save_project(req, res) {
        try {
            let project = new project_model();
            project.name = req.body.name;
            project.language = req.body.language;
            if (!utils.path_exist(constants.projects_path)) {
                utils.create_folder(constants.projects_path);
            }
            const project_path = constants.projects_path + '/' + req.body.name;
            validation_project_controller.validate_save_project(project_path, req.body.name, req.body.language);
            utils.create_folder(project_path);
            project = await project.save();
            res.json(project);
        } catch (err) {
            res.status(400).json({error: err.message});
        }
    }

    // Deletes project
    async delete_project(req, res) {
        try {
            const project_id = req.params.id;
            const files = await file_model.find({project: project_id})
            files.forEach(async file => {
                await file_model.findByIdAndRemove(file._id)
            });
            let project = await project_model.findByIdAndRemove(project_id);
            const project_folder = constants.projects_path + '/' + project.name;
            utils.delete_path(project_folder);
            res.json(project);
        } catch (err) {
            if(err.kind == 'ObjectId') {
                err.message = "Project doesn't exist in database";
            }
            res.status(400).json({error: err.message});
        }
    }
}

// Exports class
module.exports = ProjectController;
